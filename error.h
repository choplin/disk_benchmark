#ifndef __ERROR_H__
#define __ERROR_H__

#include <stdbool.h>

#define ERROR(fmt, ...) err_msg(false, __FILE__, __FUNCTION__, __LINE__, "error", fmt, ##__VA_ARGS__)
#define ERROR_E(fmt, ...) err_msg(true, __FILE__, __FUNCTION__, __LINE__, "error", fmt, ##__VA_ARGS__)
#define WARN(fmt, ...) err_msg(false, __FILE__, __FUNCTION__, __LINE__, "warnning", fmt, ##__VA_ARGS__)
#define WARN_E(fmt, ...) err_msg(true, __FILE__, __FUNCTION__, __LINE__, "warnning", fmt, ##__VA_ARGS__)

void err_msg(bool show_errno, const char *file, const char *function, int line, const char *type, const char *fmt, ...);

#endif /* __ERROR_H__ */
