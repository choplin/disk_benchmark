#ifndef __WRAPPER_H__
#define __WRAPPER_H__

#include <stdbool.h>
#include <stdint.h>

#define Open(p,m) e_Open((p),(m), __FILE__, __FUNCTION__, __LINE__)
#define Close(f) e_Close((f), __FILE__, __FUNCTION__, __LINE__)
#define Blockgetsize64(f,s) e_Blockgetsize64((f),(s), __FILE__, __FUNCTION__, __LINE__)

int e_Open(const char *pathname, int flags, const char *file, const char *function, int line);
void e_Close(int fd, const char *file, const char *function, int line);
void e_Blockgetsize64(int fd, uint64_t *size, const char *file, const char *function, int line);

#endif /* __WRAPPER_H__ */
