#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "error.h"

void err_msg(bool show_errono, const char *file, const char *function, int line, const char *type, const char *fmt, ...) {
    fprintf(stderr, "%s (%d) %s:%d:%s - ", type, getpid(), file, line, function);

    va_list ap;
    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    if (show_errono)
        fprintf(stderr, " %s(%d)", strerror(errno), errno);

    fputc('\n', stderr);
}
