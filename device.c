#include <stdlib.h>
#include <fcntl.h>

#include "device.h"
#include "random.h"
#include "wrapper.h"

device_t *init_device(char *name) {
    device_t *dev;
    int fd;

    dev = calloc(0, sizeof(device_t));
    dev->name = name;

    fd = Open(name, O_RDONLY);
    Blockgetsize64(fd, &dev->size);
    Close(fd);

    return dev;
}

void open_device(device_t *dev, int flags) {
    dev->fd = Open(dev->name, flags);
}

void close_device(device_t *dev) {
    Close(dev->fd);
}

void seek_random_pos(device_t *dev, uint64_t io_size) {
    uint64_t max_index = (dev->size - io_size) / RAW_BLOCK_SIZE;
    uint64_t rnd = random64(max_index);
    off_t pos = rnd * RAW_BLOCK_SIZE;
    lseek(dev->fd, pos, SEEK_SET);
}
