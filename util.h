#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdio.h>

#define UNUSED(x) (void)(x)

#define MILLION 1000000

#define DURATION(start, end) ((end).tv_sec - (start).tv_sec) * MILLION + ((end).tv_usec - (start).tv_usec)

static inline void show_progress(long i, long total) {
    if (i % 100 == 0) {
        printf("\r%ld / %ld", i , total);
        fflush(stdout);
    }
}

#endif /* __UTIL_H__ */
