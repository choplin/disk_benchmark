#ifndef __RANDOM_WRITE_H__
#define __RANDOM_WRITE_H__

#include "config.h"

void random_write_benchmark(const benchmark_config *config);

#endif /* __RANDOM_WRITE_H__ */
