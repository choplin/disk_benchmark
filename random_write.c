#include <string.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdlib.h>

#include "random_write.h"
#include "result.h"
#include "util.h"

void random_write_benchmark(const benchmark_config *config) {
    long i;
    device_t *dev = config->dev;
    result_t result = {0,0,0,0,0};
    long latency;
    struct timeval start, end;

    size_t size = config->size;
    char *buf = calloc(1, size);

    open_device(dev, O_WRONLY);

    for (i = 0; i < config->count; i++) {
        if (config->output == NORMAL)
            show_progress(i+1, config->count);

        seek_random_pos(dev, (uint64_t) size);

        gettimeofday(&start, NULL);
        write(dev->fd, buf, size);
        gettimeofday(&end, NULL);
        latency = DURATION(start, end);

        update_result(&result, latency);
    }

    show_result(&result, config->output);

    free(buf);
    close_device(dev);
}
