PROGRAM = benchmark
OBJS = config.o main.o write.o read.o random_write.o random_read.o device.o random.o error.o wrapper.o result.o
CFLAGS = -std=c99 -W -Wall -Werror -O2 -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE
CC=gcc

.SUFFIXES: .c .o

$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS) -o $(PROGRAM) $^

.c.o:
	$(CC) $(CFLAGS) -c $<

.PHONY: clean
clean:
	rm -rf $(PROGRAM) $(OBJS)
