#ifndef __RANDOM_READ_H__
#define __RANDOM_READ_H__

#include "config.h"

void random_read_benchmark(const benchmark_config *config);

#endif /* __RANDOM_READ_H__ */
