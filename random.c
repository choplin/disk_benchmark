#include <stdlib.h>
#include <sys/time.h>

#include "random.h"
#include "error.h"

void srandom64() {
    struct timeval now;
    if (gettimeofday(&now, NULL) < 0) {
        ERROR_E("failed to gettimeofday");
        exit(EXIT_FAILURE);
    }
    srandom(now.tv_usec);
}

uint64_t random64(uint64_t max) {
    uint64_t n;
    n = random();
    n = (n << 32) | random();
    return n % (max+1);
}
