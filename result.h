#ifndef __RESULT_H__
#define __RESULT_H__

#include "config.h"

typedef struct {
    long count;
    long sum;
    long square_sum;
    long min;
    long max;
} result_t;

void show_result(result_t *result, enum output_t output);
void update_result(result_t *result, long latency);

#endif /* __RESULT_H__ */
