#include <math.h>
#include <stdio.h>

#include "result.h"

static void normal_output(result_t *result, double avg, double variance);
static void simple_output(result_t *result, double avg, double variance);

void show_result(result_t *result, enum output_t output) {
    double avg = (double)result->sum / result->count;
    double variance = (double)result->square_sum / result->count - pow(avg, 2);

    switch (output) {
        case NORMAL:
            normal_output(result, avg, variance);
            break;
        case SIMPLE:
            simple_output(result, avg, variance);
            break;
    }
}

void update_result(result_t *result, long latency) {
    result->count += 1;
    result->sum += latency;
    result->square_sum += pow(latency, 2);
    result->min = (latency < result->min) ? latency : result->min;
    result->max = (latency > result->max) ? latency : result->max;
}

static void normal_output(result_t *result, double avg, double variance) {
    printf("\n\ncount: %ld\n \
total: %ld\n \
average: %f\n \
variance: %f\n \
min: %ld\n \
max: %ld\n",
        result->count,
        result->sum,
        avg,
        variance,
        result->min,
        result->max);
}

static void simple_output(result_t *result, double avg, double variance) {
    printf("%ld\t%ld\t%f\t%f\t%ld\t%ld\n",
        result->count,
        result->sum,
        avg,
        variance,
        result->min,
        result->max);
}
