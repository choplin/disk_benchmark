#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include <sys/ioctl.h>

#ifndef __APPLE__
#   include <linux/fs.h>
#endif

#include "wrapper.h"
#include "error.h"
#include "util.h"

static char *get_path(int fd);

int e_Open(const char *pathname, int flags, const char *file, const char *function, int line) {
    int fd;
    if((fd = open(pathname, flags)) < 0) {
        err_msg(true, file, function, line, "error", "failed to open %s", pathname);
        exit(EXIT_FAILURE);
    }
    return fd;
}

void e_Close(int fd, const char *file, const char *function, int line) {
    if(close(fd) < 0) {
        err_msg(true, file, function, line, "error", "failed to close %s", get_path(fd));
        exit(EXIT_FAILURE);
    }
}

void e_Blockgetsize64(int fd, uint64_t *size, const char *file, const char *function, int line) {
/* suppress compile error in mac... */
#ifdef __APPLE__
    UNUSED(fd);
    UNUSED(file);
    UNUSED(function);
    UNUSED(line);
    *size = 0;
#else
    if (ioctl(fd, BLKGETSIZE64, size) == -1) {
        err_msg(true, file, function, line, "error", "failed to get size of %s", get_path(fd));
        exit(EXIT_FAILURE);
    }
#endif
}

static char *get_path(int fd) {
#ifdef __APPLE__
    char filepath[PATH_MAX];
    if (fcntl(fd, F_GETPATH, filepath) != -1) {
        ERROR("failed to get path from file descriptor");
        exit(EXIT_FAILURE);
    }
    char *ret = malloc(strlen(filepath) + 1);
    strcpy(ret, filepath);
    return ret;
#else
    char src[PATH_MAX];
    char dest[PATH_MAX];
    if (snprintf(src, PATH_MAX, "/proc/self/fd/%d", fd) < 0) {
        ERROR("failed to snprintf");
        exit(EXIT_FAILURE);
    }
    ssize_t len = readlink(src, dest, PATH_MAX);
    if (len < 0) {
        ERROR("failed to readlink");
        exit(EXIT_FAILURE);
    }
    dest[len] = '\0';
    char *ret = malloc(strlen(dest) + 1);
    strcpy(ret, dest);
    return ret;
#endif
}
