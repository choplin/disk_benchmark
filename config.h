#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>

#include "device.h"

enum output_t {
    NORMAL,
    SIMPLE
};

enum io_mode_t {
    WRITE,
    READ,
    RANDOM_WRITE,
    RANDOM_READ,
    INVALID,
};

typedef struct {
    enum io_mode_t io_mode;
    enum output_t output;
    size_t size;
    long count;
    device_t *dev;
} benchmark_config;

benchmark_config *init_config();
void parse_args(int argc, char **argv, benchmark_config *config);
void print_config(benchmark_config *config);

static inline bool simple(benchmark_config *config) {
    return config->output == SIMPLE;
}

#endif
