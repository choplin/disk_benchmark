#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

#include "device.h"
#include "config.h"
#include "error.h"

static inline const char *io_mode_string(enum io_mode_t io_mode);
static inline enum io_mode_t str_to_io_mode(const char* str);

benchmark_config DEFAULT_CONFIG = {
    WRITE,
    NORMAL,
    1024,
    1000,
    NULL
};

benchmark_config *init_config() {
    benchmark_config *config;
    config = malloc(sizeof(benchmark_config));
    memcpy(config, &DEFAULT_CONFIG, sizeof(benchmark_config));
    return config;
}

void parse_args(int argc, char **argv, benchmark_config *config) {
    int optchar;
    size_t size;

    while((optchar=getopt(argc,argv,"b:c:m:s"))!=-1) {
        switch(optchar) {
            case 'b':
                size = strtol(optarg, NULL, 10);
                if (size % RAW_BLOCK_SIZE != 0) {
                    ERROR("block size must be a multiple of %d", RAW_BLOCK_SIZE);
                    exit(EXIT_FAILURE);
                }
                config->size = size;
                break;
            case 'c':
                config->count = strtol(optarg, NULL, 10);
                break;
            case 'm':
                config->io_mode = str_to_io_mode(optarg);
                if (config->io_mode == INVALID) {
                    ERROR("invalid io mode: %s", optarg);
                    exit(EXIT_FAILURE);
                }
                break;
            case 's':
                config->output = SIMPLE;
                break;
            default:
                ERROR("argument parse error");
                exit(EXIT_FAILURE);
        }
    }

    if (argc == optind) {
        ERROR("device must be specifed");
        exit(EXIT_FAILURE);
    }

    config->dev = init_device(argv[optind]);
}

void print_config(benchmark_config *config) {
    printf("config:\n\
    type: %s\n\
    device: %s\n\
    device size: %lld\n\
    size: %ld\n\
    count: %ld\n\
\n",
        io_mode_string(config->io_mode),
        config->dev->name,
        (long long unsigned int) config->dev->size,
        config->size,
        config->count);
}

static inline const char *io_mode_string(enum io_mode_t io_mode) {
    static const char *strings[] = {
        "write",
        "read",
        "random write",
        "random read"
    };

    return strings[io_mode];
}

static inline enum io_mode_t str_to_io_mode(const char* str) {
    if (strcmp(str, "w") == 0) {
        return WRITE;
    } else if (strcmp(str, "write") == 0) {
        return WRITE;
    } else if (strcmp(str, "r") == 0) {
        return READ;
    } else if (strcmp(str, "read") == 0) {
        return READ;
    } else if (strcmp(str, "rw") == 0) {
        return RANDOM_WRITE;
    } else if (strcmp(str, "randwrite") == 0) {
        return RANDOM_WRITE;
    } else if (strcmp(str, "rr") == 0) {
        return RANDOM_READ;
    } else if (strcmp(str, "randread") == 0) {
        return RANDOM_READ;
    } else {
        return INVALID;
    }
}
