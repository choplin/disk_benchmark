#ifndef __DEVICE_H__
#define __DEVICE_H__

#include <stdint.h>
#include <sys/types.h>
#include <unistd.h>

#define RAW_BLOCK_SIZE 512

typedef struct {
    /* device name */
    char *name;
    /* file descriptor */
    int fd;
    /* device size in byte */
    uint64_t size;
} device_t;

device_t *init_device(char *name);
void open_device(device_t *dev, int flags);
void close_device(device_t *dev);

void seek_random_pos(device_t *dev, uint64_t io_size);

#endif /* __DEVICE_H__ */
