#include <stdlib.h>
#include <stdio.h>

#include "write.h"
#include "read.h"
#include "random_read.h"
#include "random_write.h"
#include "config.h"
#include "random.h"

#define clear_line printf("\033[2K")

int main(int argc, char **argv)
{
    benchmark_config *config;

    srandom64();

    config = init_config();
    parse_args(argc, argv, config);

    if (config->output == NORMAL) {
        print_config(config);
    }

    switch(config->io_mode) {
        case WRITE:
            write_benchmark(config);
            break;
        case READ:
            read_benchmark(config);
            break;
        case RANDOM_WRITE:
            random_write_benchmark(config);
            break;
        case RANDOM_READ:
            random_read_benchmark(config);
            break;
        case INVALID:
            fprintf(stderr, "invalid\n");
            exit(EXIT_FAILURE);
    }

    exit(EXIT_SUCCESS);
}
