#ifndef __RANDOM_H__
#define __RANDOM_H__

#include <stdint.h>

void srandom64();
uint64_t random64(uint64_t max);

#endif /* __RANDOM_H__ */
