#ifndef __READ_H__
#define __READ_H__

#include "config.h"

void read_benchmark(const benchmark_config *config);

#endif /* __READ_H__ */
